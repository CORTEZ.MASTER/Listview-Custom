package com.example.cortez.listview_custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

class PizzariaAdapter extends ArrayAdapter<Pizzaria> {

    private final Context context;
    private final ArrayList<Pizzaria> elementos;

    public PizzariaAdapter(Context context, ArrayList<Pizzaria> elementos) {
        super(context, R.layout.linha, elementos);
        this.context = context;
        this.elementos = elementos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.linha, parent, false);

        TextView nomeEscola = (TextView) rowView.findViewById(R.id.nome);
        TextView descricao = (TextView) rowView.findViewById(R.id.descricao);
        ImageView imagem = (ImageView) rowView.findViewById(R.id.imagem);

        nomeEscola.setText(elementos.get(position).getNome());
        descricao.setText(elementos.get(position).getDescricao());
        imagem.setImageResource(elementos.get(position).getImagem());

        return rowView;
    }
}