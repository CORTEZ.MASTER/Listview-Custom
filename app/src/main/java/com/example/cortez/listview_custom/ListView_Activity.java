package com.example.cortez.listview_custom;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class ListView_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_);

        //Deixar celular somente na vertical
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        ListView lista = (ListView) findViewById(R.id.list_pizzas);
        ArrayAdapter adapter = new PizzariaAdapter(this, adicionarPizzas());
        lista.setAdapter(adapter);

    }

    private ArrayList<Pizzaria> adicionarPizzas() {
        ArrayList<Pizzaria> pizzas = new ArrayList<Pizzaria>();

        Pizzaria p = new Pizzaria("1. Pizza de Calabresa",
                "Massa, molho e calabresa", R.drawable.piz01);
                pizzas.add(p);

        p = new Pizzaria("2. Pizza a Moda da Casa",
                "Massa, molho, queijo e tudo que há de bom.", R.drawable.piz02);
        pizzas.add(p);

        p = new Pizzaria("3. Pizza a Portuguesa",
                "Massa, molho, ovo, presunto e queijo.", R.drawable.piz03);
        pizzas.add(p);

        p = new Pizzaria("4. Pizza a Nspolitana",
                "Massa, molho, queijo e manjericão.", R.drawable.piz04);
        pizzas.add(p);

        p = new Pizzaria("5. Pizza a Putanesca",
                "Massa, molho, queijo e molho pesto", R.drawable.piz05);
        pizzas.add(p);

        p = new Pizzaria("6. Pizza Marguerita",
                "Massa, molho, queijo mussarela", R.drawable.piz06);
        pizzas.add(p);

        p = new Pizzaria("7. Pizza de Frango com Catupiry",
                "Massa, molho, queijo e frango com catupiry", R.drawable.piz07);
        pizzas.add(p);

        p = new Pizzaria("8. Pizza de Presunto",
                "Massa, molho, queijo e presunto.", R.drawable.piz08);
        pizzas.add(p);

        p = new Pizzaria("9. Pizza de Peperroni",
                "Massa, molho, queijo e peperroni", R.drawable.piz09);
        pizzas.add(p);

        p = new Pizzaria("10. Pizza da Galera",
                "Massa, molho, queijo e tudo que for possivel colocar.", R.drawable.piz10);
        pizzas.add(p);

        return pizzas;
    }
}
